# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import os

# Diccionario que lleva el control de los puntos
puntos = { "A" : 1, "E" : 1, "L" : 1, "I" : 1, "N" : 5, "O" : 1, "R" : 1,
            "S" : 1, "T" : 1, "U" : 1, "D" : 2, "G" : 2, "B" : 3, "C" : 3,
            "M" : 3, "P" : 3, "F" : 4, "H" : 4, "V" : 3, "Y" : 4, "CH" : 5,
            "Q" : 5, "J" : 8, "LL" : 8, "Ñ" : 8, "RR" : 8, "X" :8, "Z" : 10 }


# Procedieminto para imprimir la matriz
def imprimir(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end=' ')
        print()



# Procedimiento para generar la matriz principal del juego
def matriz(jugadores):
    matriz = []
    print("\n")
    for i in range(15):
        matriz.append([])
        for j in range(15):
            matriz[i].append("| ")
    imprimir(matriz)


# Procedimeinto para ver la cantidad de jugadores
def jugador():
    print("Para empezar el juego se necesitan de 2 a 4 jugadores.")
    jugadores = int(input("Ingrese la cantidad de jugadores: "))
    try:
# Para evaluar que el usuario ingrese la cantidad correcta de jugadores
        if 2 <= jugadores <= 4:
            matriz(jugadores)
        else:
            print("Ingrese la cantidad incorrecta de jugadores")
            jugador()
# Para evaluar que el usuario ingrese un numero
    except ValueError:
        print("Debe ingresar un numero")
        jugador()


# Función principal o main
if __name__ == "__main__":
    os.system('clear')
    print("Bienvenido al juego de Scrabble")
    jugador()
